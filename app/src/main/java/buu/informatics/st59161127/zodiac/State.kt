package buu.informatics.st59161127.zodiac


import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import buu.informatics.st59161127.zodiac.databinding.FragmentStateBinding
import kotlinx.android.synthetic.main.fragment_add_state.*
import kotlinx.android.synthetic.main.fragment_state.*

/**
 * A simple [Fragment] subclass.
 */
class State : Fragment() {
    private lateinit var binding: FragmentStateBinding
    private lateinit var zodiacData:ZodiacModels

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        zodiacData = ViewModelProviders.of(this).get(ZodiacModels::class.java)
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_state,
            container,false)
        Log.i("check","open app")
        showData()
        binding.startBtn.setOnClickListener { View ->
                View.findNavController().navigate(R.id.action_state_to_addState)
        }
        binding.edittextBtn.setOnClickListener {View ->
            if (EmptyEdittext()) {
                View.findNavController().navigate(R.id.action_state_to_addState)
            }
        }
        binding.deleteBtn.setOnClickListener {
            zodiacData.delete()
            setAfterDelete()
        }
        setHasOptionsMenu(true)
        return binding.root
    }

    fun setAfterDelete(){
        binding.showText.text = ""
        binding.showGenderText.text = ""
        binding.birthShowText.text = ""
        binding.zodiaShowText.text = ""
        binding.startBtn.isEnabled = true
        binding.startBtn.setBackgroundColor(Color.parseColor("#000000"))
    }
    fun showData(){
        Log.i("check","come showdata")
        zodiacData.allUser.observe(this,
            Observer {data  -> data.map {
                data.forEach {
                    Log.i("check",it.toString())
                }
                binding.showText.text = it.name
                binding.showGenderText.text = it.gender
                binding.birthShowText.text = it.birthday
                binding.zodiaShowText.text = it.zodiac
                binding.startBtn.isEnabled = false
                binding.startBtn.setBackgroundColor(Color.parseColor("#a9a9a9"))
                Log.i("check","get data")
            }
            })
    }
    fun EmptyEdittext():Boolean {
        if (show_text.text.isEmpty()){
            Toast.makeText(activity, "Please add information.", Toast.LENGTH_LONG).show()
            return false
        }else if (show_gender_text.text.isEmpty()){
            Toast.makeText(activity, "Please add information.", Toast.LENGTH_LONG).show()
            return false
        }else if (birth_show_text.text.isEmpty()){
            Toast.makeText(activity, "Please add information.", Toast.LENGTH_LONG).show()
            return false
        }else if (zodia_show_text.text.isEmpty()){
            Toast.makeText(activity, "Please add information.", Toast.LENGTH_LONG).show()
            return false
        }else{
            return true
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.option_menu, menu)
        if (null == getShareIntent().resolveActivity(activity!!.packageManager)) {
            // hide the menu item if it doesn't resolve
            menu?.findItem(R.id.shareFragment)?.setVisible(false)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item!!.itemId) {
            R.id.shareFragment -> shareSuccess()
        }
        return NavigationUI.onNavDestinationSelected(item,
            view!!.findNavController()) || super.onOptionsItemSelected(item)
    }
    private fun getShareIntent() : Intent {
        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.setType("text/plain")
            .putExtra(Intent.EXTRA_TEXT,"Share")
        return shareIntent
    }
    private fun shareSuccess() {
        startActivity(getShareIntent())
    }

}
