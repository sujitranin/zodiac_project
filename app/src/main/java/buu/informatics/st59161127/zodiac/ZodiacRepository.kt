package buu.informatics.st59161127.zodiac

import androidx.lifecycle.LiveData
import buu.informatics.st59161127.zodiac.database.ZodiacDatabaseDAO
import buu.informatics.st59161127.zodiac.database.zodiacModels

class ZodiacControllerRepository (private val zodiacDAO: ZodiacDatabaseDAO) {
    val allzodiac: LiveData<List<zodiacModels>> = zodiacDAO.getAllzodiac()

    fun insert(zodi: zodiacModels){
        zodiacDAO.insert(zodi)
    }
    fun findByName(item: String): LiveData<List<zodiacModels>>
            = zodiacDAO.find(item)

    fun clear(){
        zodiacDAO.clear()
    }
}
