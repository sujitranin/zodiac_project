package buu.informatics.st59161127.zodiac

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import buu.informatics.st59161127.zodiac.database.*
import kotlinx.coroutines.launch

class ZodiacModels(application: Application) : AndroidViewModel(application){
    private val repository: ZodiacControllerRepository
    val allUser: LiveData<List<zodiacModels>>

    init {
        val wordDAO = ZodiacDatabase.getDatabase(application.applicationContext).ZodiacDatabaseDAO()
        repository = ZodiacControllerRepository(wordDAO)
        allUser = repository.allzodiac
    }

    fun insert(zodi: zodiacModels) = viewModelScope.launch {
        repository.insert(zodi)
    }

    fun delete(){
        repository.clear()
    }

}