package buu.informatics.st59161127.zodiac


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import buu.informatics.st59161127.zodiac.database.zodiacModels
import buu.informatics.st59161127.zodiac.databinding.FragmentAddStateBinding
import kotlinx.android.synthetic.main.fragment_add_state.*
import kotlinx.android.synthetic.main.fragment_state.*

/**
 * A simple [Fragment] subclass.
 */
class AddState : Fragment() {
    private lateinit var binding: FragmentAddStateBinding
    //เรียกใช้database
    private lateinit var zodiacData:ZodiacModels

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?

    ): View? {
        //เรียกใช้database
        zodiacData = ViewModelProviders.of(this).get(ZodiacModels::class.java)

        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_add_state,
            container,false)
            loadState()
            binding.saveBtn.setOnClickListener {View ->
               if ( EmptytextEditadd()){
                   View.findNavController().navigate(R.id.action_addState_to_state)
               }
            }

        return binding.root
    }

    private fun loadState() {
        zodiacData.allUser.observe(this,
            Observer {data  -> data.map {
               data.forEach{
                   if (data.isNotEmpty()){
                       Log.i("check work","pass1")
                      setState()
                   }
               }
            }
            })
    }

    private fun setState() {
        zodiacData.allUser.observe(this,
            Observer {data  -> data.map {
                Log.i("check work","pass2")
                binding.nameTextEdit.setText(it.name)
                binding.grnderTextEdit.setText(it.gender)
                binding.birthdayTextEdit.setText(it.birthday)
                binding.zodiEdteText.setText(it.zodiac)
            } }
        )
    }



    fun addState() {
        var name = binding.nameTextEdit.text.toString()
         var gender = this.grnder_text_edit.text.toString()
         var birthday = this.birthday_text_edit.text.toString()
         var zodiac = this.zodi_edte_text.text.toString()
         zodiacData.insert(zodiacModels(
             0,
             name,
             gender,
             birthday,
             zodiac
         ))
    }
    fun EmptytextEditadd():Boolean {
        if (name_text_edit.text.isEmpty()){
            Toast.makeText(activity, "Please enter your information", Toast.LENGTH_LONG).show()
            return false
        }
        else if (grnder_text_edit.text.isEmpty()){
            Toast.makeText(activity, "Please enter your information", Toast.LENGTH_LONG).show()
            return false
        }
        else if (birthday_text_edit.text.isEmpty()){
            Toast.makeText(activity, "Please enter your information", Toast.LENGTH_LONG).show()
            return false
        }
        else if (zodi_edte_text.text.isEmpty()){
            Toast.makeText(activity, "Please enter your information", Toast.LENGTH_LONG).show()
            return false
        }else{
            addState()
            return true
        }
    }
}
