package buu.informatics.st59161127.zodiac.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = arrayOf(zodiacModels::class), version = 1, exportSchema = false)
abstract class ZodiacDatabase : RoomDatabase() {

    abstract fun ZodiacDatabaseDAO(): ZodiacDatabaseDAO
    companion object{
        @Volatile
        private var INSTANCE: ZodiacDatabase? = null

        fun getDatabase(
            context: Context
        ): ZodiacDatabase{

            return INSTANCE ?: synchronized(this){
                val instance = Room.databaseBuilder (
                    context.applicationContext,
                    ZodiacDatabase::class.java, "zodiac_database"
                ).allowMainThreadQueries().build()
                INSTANCE = instance
                instance
            }
        }
    }
}