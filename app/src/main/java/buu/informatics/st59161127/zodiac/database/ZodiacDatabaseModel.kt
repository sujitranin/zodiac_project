package buu.informatics.st59161127.zodiac.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "list_all_zodiac")
data class zodiacModels(
    @PrimaryKey(autoGenerate = true)
    var zodiacID: Long = 0L,

    @ColumnInfo(name = "name")
    val name: String,

    @ColumnInfo(name = "gender")
    val gender: String,

    @ColumnInfo(name = "birthday")
    val birthday: String,

    @ColumnInfo(name = "zodiac")
    val zodiac: String
)
