package buu.informatics.st59161127.zodiac.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface ZodiacDatabaseDAO {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(zodi: zodiacModels)

    @Query("SELECT * FROM list_all_zodiac ORDER BY zodiacID DESC LIMIT 1")
    fun getAllzodiac(): LiveData<List<zodiacModels>>

    @Query("SELECT * FROM list_all_zodiac WHERE name = :name ")
    fun getOnezodiac(name: String): zodiacModels

    @Query("SELECT * FROM list_all_zodiac WHERE name LIKE :name LIMIT 1")
    fun find(name: String): LiveData<List<zodiacModels>>

    @Query("DELETE FROM list_all_zodiac")
    fun clear()


}